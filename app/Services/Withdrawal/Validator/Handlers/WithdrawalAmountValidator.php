<?php


namespace App\Services\Withdrawal\Validator\Handlers;


use App\Services\Withdrawal\Validator\Contracts\Validator;
use App\Services\Withdrawal\Validator\Exceptions\WithdrawalAmountException;
use App\Services\Withdrawal\WithdrawalRequest;

class WithdrawalAmountValidator extends Validator
{

    protected function process(WithdrawalRequest $request)
    {
        $min_amount = config('constants.withdrawal.amount_min');
        if (intval($request->getAmount()) < $min_amount) {
            throw new WithdrawalAmountException('مبلغ درخواستی کمتر از حداقل مبلغ تعیین شده می باشد.');
        }
        return true;
    }
}