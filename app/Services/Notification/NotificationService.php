<?php


namespace App\Services\Notification;


use App\Services\Notification\Exceptions\NotFoundNotificationProviderException;
use App\Services\Notification\Providers\EmailProvider;
use App\Services\Notification\Providers\SmsProvider;
use App\Services\Notification\Providers\SocketProvider;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;

class NotificationService
{

    public function send(array $args)
    {

        $provider = $args['type'];
        $providerHandler = $this->getProvider($provider);
        return $providerHandler->send($args);

    }

    private function getProvider(int $type)
    {
        $provider = null;
        $providerType = NotificationType::getTypeHandler($type);
        $providerBasenameSpace = ucfirst(strtolower($providerType));
        $providerClass = 'App\\Services\\Notification\\Providers\\' . $providerBasenameSpace . '\\' . $providerBasenameSpace . 'Provider';
        if (!class_exists($providerClass)) {
            throw new NotFoundNotificationProviderException('notification provider was not found!');
        }
        $provider = new $providerClass;
        return $provider;
    }


}