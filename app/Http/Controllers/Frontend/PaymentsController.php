<?php

namespace App\Http\Controllers\Frontend;

use App\Repositories\Eloquent\Payment\PaymentType;
use App\Services\Payment\PaymentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{

    /**
     * @var PaymentService
     */
    private $paymentService;

    public function __construct(PaymentService $paymentService)
    {

        $this->paymentService = $paymentService;
    }

    public function start(Request $request)
    {
//        $paymentKey = $request->payment_key;
        return $this->paymentService->doPayment(1,PaymentType::CASH);
    }

    public function verify(Request $request, $payment_code)
    {
        $params = $request->all();
        $params['paymentCode'] = $payment_code;
        $verifyResult = $this->paymentService->verifyPayment($params);
        return view('payment.verify', compact('verifyResult'));
    }
}
