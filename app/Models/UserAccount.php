<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\UserAccount\UserAccountPresenter;
use Illuminate\Database\Eloquent\Model;

class UserAccount extends Model
{
    use Presentable;

    const ACTIVE = 1;
    const INACTIVE = 0;

    protected $primaryKey = 'user_account_id';

    protected $guarded = ['user_account_id'];

    protected $presenter = UserAccountPresenter::class;

    /*Relations*/

    public static function getStatuses()
    {
        return [
            self::ACTIVE   => 'فعال',
            self::INACTIVE => 'غیر فعال'
        ];
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_account_user_id');
    }

    /*End Relations*/

    public function withdrawals()
    {
        return $this->hasMany(Withdrawal::class, 'withdrawal_user_account_id');
    }

}
