<?php

namespace App\Models;

use App\Presenters\Contracts\Presentable;
use App\Presenters\Withdrawal\WithdrawalPresenter;
use Illuminate\Database\Eloquent\Model;

class Withdrawal extends Model
{
    use Presentable;
    const PENDING = 1;
    const DONE = 2;
    const REJECTED = 3;

    protected $primaryKey = 'withdrawal_id';

    protected $guarded = ['withdrawal_id'];

    protected $presenter = WithdrawalPresenter::class;

    /*Relations*/

    public static function getStatuses()
    {
        return [
            self::PENDING  => 'در حال بررسی',
            self::DONE     => 'انجام شده',
            self::REJECTED => 'رد شده'
        ];
    }

    public function gateway()
    {
        return $this->belongsTo(Gateway::class, 'withdrawal_gateway_id');
    }

    /*End Relations*/

    public function account()
    {
        return $this->belongsTo(UserAccount::class, 'withdrawal_user_account_id');
    }

    public function updateStatus($status)
    {
        $this->withdrawal_status = $status;
        $this->save();
    }
}
